package com.example.feign.controller;

import com.example.feign.model.LoginRequest;
import com.example.feign.service.users.UsersService;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

	@Autowired
	private UsersService usersService;

	@GetMapping("users")
	public ResponseEntity<ResultResponse> getUsers(HttpServletRequest http) {
		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		return usersService.getDetailUsers(accessToken);
	}

	@PostMapping("users/login")
	public ResponseEntity<ResultResponse> loginUsers(HttpServletRequest http, @RequestBody LoginRequest request) {
		String accessToken = http.getHeader(GlobalConstants.HEADER_TOKEN);

		return usersService.userLogin(accessToken, request);
	}
}
