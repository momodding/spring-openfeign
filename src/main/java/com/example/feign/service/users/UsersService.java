package com.example.feign.service.users;

import com.example.feign.model.LoginRequest;
import com.example.feign.utils.FeignConfiguration;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.GlobalConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "users-service",
		url = "${microservice.users}",
		configuration = FeignConfiguration.class, fallback = UsersServiceFallback.class)
public interface UsersService {

	@RequestMapping(method = RequestMethod.GET, value = "/frontoffice/user/me",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultResponse> getDetailUser(@RequestHeader(GlobalConstants.HEADER_TOKEN) String accessToken);

	@GetMapping(value = "/frontoffice/user/me",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultResponse> getDetailUsers(@RequestHeader(GlobalConstants.HEADER_TOKEN) String accessToken);

	@PostMapping(value = "/auth/login",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultResponse> userLogin(@RequestHeader(GlobalConstants.HEADER_TOKEN) String accessToken, LoginRequest loginRequest);
}

