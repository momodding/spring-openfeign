package com.example.feign.service.users;

import com.example.feign.model.LoginRequest;
import id.investree.core.base.MetaResponse;
import id.investree.core.base.ResultResponse;
import id.investree.core.constant.StatusCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UsersServiceFallback implements UsersService {

	@Override
	public ResponseEntity<ResultResponse> getDetailUser(String accessToken) {
		MetaResponse metaResponse = new MetaResponse();
		metaResponse.code = HttpStatus.OK.value();
		metaResponse.message = "failed to response";
		ResultResponse resultResponse = new ResultResponse();
		resultResponse.status = StatusCode.ERROR.name();
		resultResponse.meta = metaResponse;
		return new ResponseEntity(resultResponse, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResultResponse> getDetailUsers(String accessToken) {
		MetaResponse metaResponse = new MetaResponse();
		metaResponse.code = HttpStatus.OK.value();
		metaResponse.message = "failed to response";
		ResultResponse resultResponse = new ResultResponse();
		resultResponse.status = StatusCode.ERROR.name();
		resultResponse.meta = metaResponse;
		return new ResponseEntity(resultResponse, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<ResultResponse> userLogin(String accessToken, LoginRequest loginRequest) {
		MetaResponse metaResponse = new MetaResponse();
		metaResponse.code = HttpStatus.OK.value();
		metaResponse.message = "failed to response";
		ResultResponse resultResponse = new ResultResponse();
		resultResponse.status = StatusCode.ERROR.name();
		resultResponse.meta = metaResponse;
		return new ResponseEntity(resultResponse, HttpStatus.OK);
	}
}
