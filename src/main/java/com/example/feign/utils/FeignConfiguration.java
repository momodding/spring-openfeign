package com.example.feign.utils;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import id.investree.core.constant.GlobalConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfiguration {

	@Value("${headers-value.key}")
	private String accessKey;

	@Bean
	public RequestInterceptor requestInterceptor() {
		return new RequestInterceptor() {
			@Override
			public void apply(RequestTemplate requestTemplate) {
				requestTemplate.header(GlobalConstants.HEADER_KEY,accessKey);
				requestTemplate.header(GlobalConstants.HEADER_SIGNATURE, GlobalConstants.IGNORED);
				requestTemplate.header(GlobalConstants.HEADER_TIMESTAMP, GlobalConstants.IGNORED);
			}
		};
	}
}
