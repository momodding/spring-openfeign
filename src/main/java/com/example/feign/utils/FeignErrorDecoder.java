package com.example.feign.utils;

import feign.Response;
import feign.codec.ErrorDecoder;
import id.investree.core.exception.AppException;
import id.investree.core.exception.DataNotFoundException;

public class FeignErrorDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String methodeKey, Response response) {

		switch (response.status()){
			case 400:
			case 500:
				return new AppException(response.reason());
			case 404:
				return new DataNotFoundException();
			default:
				return new Exception("Generic error");
		}
	}
}
