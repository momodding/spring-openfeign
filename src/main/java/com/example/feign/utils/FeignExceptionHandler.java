package com.example.feign.utils;

import feign.FeignException;
import id.investree.core.base.ResultResponse;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class FeignExceptionHandler {

	@ExceptionHandler(FeignException.BadRequest.class)
	public ResponseEntity<ResultResponse> handleFeignStatusException(FeignException e, HttpServletResponse response) {
		response.setStatus(e.status());
		System.out.println(e.contentUTF8());
		Map<String, Object> json;
		try {
			json = new JSONObject(e.contentUTF8()).toMap();

		} catch (Exception ex) {
			json = new HashMap<>();
		}
		return new ResponseEntity(json, HttpStatus.valueOf(e.status()));
	}
}
